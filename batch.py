from . import threadsafe

from googleapiclient.http import BatchHttpRequest

from concurrent.futures import ThreadPoolExecutor
import threading
import queue

from collections import namedtuple

import logging


__all__ = ['ChunkedTasksPoolScheduler', 'GoogleBatchScheduler']


EmbededException = namedtuple('EmbededException', 'exception')


def Idx(idx=0):
    while True:
        yield idx
        idx += 1


class ChunkedTasksPoolScheduler:
    Ref = namedtuple('Ref', ('idx', 'src'))
    Task = namedtuple('Task', ('ref', 'payload'))
    Result = namedtuple('Result', ('task', 'payload'))
    _Executor = ThreadPoolExecutor
    _POOLSIZE = 4
    _CHUNKSIZE = 50


    def __init__(S, chunkSize=None, pool=None):
        S._logger = logging.getLogger(__name__).getChild(type(S).__name__)

        if chunkSize is None:
            S.chunkSize = int(S._CHUNKSIZE)
        else:
            S.chunkSize = int(chunkSize)
        assert S.chunkSize > 0

        if pool is None:
            S.pool = S._Executor(S._POOLSIZE)
        elif isinstance(pool, int):
            S.pool = S._Executor(S._POOLSIZE)
        else:
            raise TypeError(pool)

        S.lockFutures = threading.RLock()
        S.lockSubmit = threading.RLock()
        S.lockReceive = threading.RLock()

        S.completed = threading.Event()

        S._idx = Idx()

        S._submitted = 0
        S._unprocessed = []
        S.futures = []
        S._received = 0
        S.results = queue.Queue()

        S._errors = 0


    def __str__(S):
        return '<%s %s>' %(type(S).__name__, ' '.join(f'{k}:{v}' for k,v in S.status().items()))
    __repr__ = __str__


    def status(S):
        return {
            'submitted': S._submitted,
            'unprocessed': len(S._unprocessed),
            'received': S._received,
            'errors': S._errors,
            }


    def cancel(S):
        with S.lockFutures:
            for future in S.futures:
                future.cancel()


    def iter(S, block=True, timeout=None):
        while True:
            try:
                result = S.results.get(block, timeout)
            except Exception as E:
                S._thread_errors += 1
                yield EmbededException(E)
            else:
                yield result
            if S.completed.is_set():
                break
    __iter__ = iter


    def submit(S, srcs):
        tasks = []
        for src in srcs:
            ref = S.Ref(next(S._idx), src)
            tasks.append(S.Task(ref, S._prepareRef(ref)))
        with S.lockSubmit:
            S._unprocessed.extend(tasks)
            S._submitted += len(tasks)
            S.completed.clear()
            for _tasks in S._chunk():
                S._process(_tasks)
        return tasks


    def reSubmit(S, task):
        with S.lockSubmit:
            S._unprocessed.append(task)
            for _tasks in S._chunk():
                S._process(_tasks)


    def _prepareRef(S, ref):
        # override
        return None


    def _chunk(S):
        chunkSize = int(S.chunkSize)
        assert chunkSize > 0
        while len(S._unprocessed) >= chunkSize:
            tasks = S._unprocessed[:chunkSize]
            del S._unprocessed[:chunkSize]
            yield tasks


    def flush(S):
        with S.lockSubmit:
            for tasks in S._chunk():
                S._process(tasks)
            if S._unprocessed:
                unprocessed = list(S._unprocessed)
                S._unprocessed.clear()
                S._process(unprocessed)


    def _process(S, tasks):
        def futureProcess():
            try:
                return S._batch(tasks)
            except Exception as E:
                S._logger.exception('error in thread')
                raise
        future = S.pool.submit(futureProcess)
        future.tasks = tasks
        with S.lockFutures:
            S.futures.append(future)


    def _batch(S, tasks):
        # Dummy
        for task in tasks:
            S._receive(S._makeResult(task, None))


    def _makeResult(S, task, data):
        return S.Result(task, data)


    # TODO: add receivers?
    def _receive(S, result):
        S.results.put(result)
        with S.lockReceive:
            S._received += 1
            with S.lockSubmit:
                if S._received == S._submitted:
                    S._onCompleted()


    def _onCompleted(S):
        S._logger.debug('completed')
        S.completed.set()


class GoogleBatchScheduler(ChunkedTasksPoolScheduler):

    BatchResult = namedtuple('BatchResult', ('id', 'response', 'exception'))
    _BatchHttpRequest = BatchHttpRequest


    def __init__(S, chunkSize=None, pool=None):
        threadsafe.patch()
        super().__init__(chunkSize, pool)
        S._batchErrors = 0
        S.retried = []


    def status(S):
        status = super().status()
        status['batchErrors'] = S._batchErrors
        return status


    def _prepareRef(S, ref):
        id = S._prepareId(ref)
        request = S._prepareRequest(ref)
        callback = S._prepareCallback(ref)
        return (request, callback, id)


    def _prepareId(S, ref):
        ''' must be str or None '''
        return None


    def _prepareRequest(S, ref): # override
        return ref.src


    def _prepareCallback(S, ref): # override
        ''' callback(task, data)
            data may be (id, response, exception)
        '''
        return None


    def _batch(S, tasks):
        batch = S._BatchHttpRequest()
        for task in tasks:
            request, callback, id = task.payload
            callback = S._makeCallback(task, callback)
            batch.add(request, callback, id)
        return batch.execute()
        # XXX
        # import httplib2; http = httplib2.Http();
        # return batch.execute(http=http)


    def _makeCallback(S, task, callback):
        if callback is None:
            def _callback(*data):
                S._receive(S._makeResult(task, data))
        else:
            def _callback(*data):
                if not callback(task, data):
                    S._receive(S._makeResult(task, data))
        return _callback


    def _makeResult(S, task, data):
        return S.Result(task, S.BatchResult(*data))


    def _retry(S, result):
        S._logger.warning(f'Task {result.task.ref.idx} for {result.task.ref.src!r} resubmitting... (will not flush automatically, though!)')
        S.retried.append(result)
        S.reSubmit(result.task)


    def _logResult(S, result):
        if result.payload.exception:
            S._logger.debug(f'Task {result.task.ref.idx} for {result.task.ref.src!r} ERROR: {result.payload.exception}')
        else:
            S._logger.debug(f'Task {result.task.ref.idx} for {result.task.ref.src!r} DONE')


    def _receive(S, result):
        S._logResult(result)
        if result.payload.exception:
            S._batchErrors += 1
            if getattr(result.payload.exception, 'resp', {}).get('status') == '429':
                S._retry(result)
                return
        super()._receive(result)


_ = GoogleBatchScheduler
