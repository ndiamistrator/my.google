from .service import Service
from .batch import GoogleBatchScheduler

import logging


__all__ = ['AVAILABLE_SCOPES', 'Gmail', 'getMessageSummary', 'BatchMessageGetter']


AVAILABLE_SCOPES = [
    'https://mail.google.com/',
    'https://www.googleapis.com/auth/gmail.compose',
    'https://www.googleapis.com/auth/gmail.insert',
    'https://www.googleapis.com/auth/gmail.labels',
    'https://www.googleapis.com/auth/gmail.modify',
    'https://www.googleapis.com/auth/gmail.readonly',
    'https://www.googleapis.com/auth/gmail.send',
    'https://www.googleapis.com/auth/gmail.settings.basic',
    'https://www.googleapis.com/auth/gmail.settings.sharing',

    'https://www.googleapis.com/auth/gmail.metadata',
    'https://www.googleapis.com/auth/gmail.addons.current.action.compose',
    'https://www.googleapis.com/auth/gmail.addons.current.message.action',
    'https://www.googleapis.com/auth/gmail.addons.current.message.metadata',
    'https://www.googleapis.com/auth/gmail.addons.current.message.readonly',
    ]


logger = logging.getLogger(__name__)


class Gmail(Service):

    SERVICE_ID = {'serviceName':'gmail', 'version':'v1'}
    SCOPES = ['https://mail.google.com/']

    MAX_BATCH_DELETE = 1000 # gmail limit
    MAX_SEARCH_PER_PAGE = 500 # gmail limit


    def getMessageSummary(S, messageId, userId='me', _messagesAPI=None):
        if _messagesAPI is None:
            _messagesAPI = S.service.users().messages()
        request = _messagesAPI.get(userId=userId, id=messageId)
        response = request.execute()
        return getMessageSummary(response)


    def search(S, query, userId='me', perPage=None, maxPages=0, nextPageToken=None):
        for response in S.searchPaged(query, userId=userId, perPage=perPage, maxPages=maxPages, nextPageToken=nextPageToken):
            yield from response['messages'] # {'id': ..., 'threadId': ...}



    def searchPaged(S, query, userId='me', perPage=None, maxPages=0, nextPageToken=None):
        S._logger.debug(f'starting query: {query!r} (perPage={perPage}, maxPages={maxPages})')
        request = None
        page = 1
        if perPage is None:
            perPage = S.MAX_SEARCH_PER_PAGE
        maxPages = int(maxPages)
        count = 0

        while True:
            request = S.service.users().messages().list(userId=userId, q=query, maxResults=perPage, pageToken=nextPageToken)
            response = request.execute()
            messages = response.get('messages', [])

            if not messages:
                break

            _count = len(messages)
            count += _count
            S._logger.debug(f'found {_count} message{"s" if _count>1 else ""} in page {page}...')

            yield response

            if page == maxPages:
                break

            nextPageToken = response.get('nextPageToken', None)
            if nextPageToken is None:
                break

            page += 1

        if not count:
            S._logger.debug(f'TOTAL: found no messages')
        else:
            S._logger.debug(f'TOTAL: found {count} message{"s" if count>1 else ""} in {page} page{"s" if page>1 else ""}')


    def batchDelete(S, messageIds, userId='me', maxBatch=None):
        if maxBatch is None:
            maxBatch = S.MAX_BATCH_DELETE
        messageIds = list(messageIds)
        while messageIds:
            request = S.service.users().messages().batchDelete(userId=userId, body={'ids':messageIds[:maxBatch]})
            response = request.execute()
            yield response
            del messageIds[:maxBatch]


    def emptyTrash(S, userId='me'):
        _messagesAPI = S.service.users().messages()
        count = 0
        while True:
            messageIds = []
            for response in S.searchPaged('in:Trash', userId=userId, perPage=500, maxPages=2):
                _messageIds = [message['id'] for message in response.get('messages', [])]
                if not _messageIds:
                    break
                messageIds.extend(_messageIds)
                date, snippet = S.getMessageSummary(_messageIds[-1], _messagesAPI=_messagesAPI)
                S._logger.debug(f'Last in page: [{date}] {snippet!r}')
            if not messageIds:
                break
            S._logger.info('batchDelete: %s' %(list(S.batchDelete(messageIds, userId=userId)),))
        if not count:
            S._logger.debug(f'TOTAL: deleted no messages')
        else:
            S._logger.debug(f'TOTAL: deleted {count} message{"s" if count>1 else ""}')


    def getBatchMessageGetter(S, batchSize=None, pool=None):
        return BatchMessageGetter(S, batchSize=batchSize, pool=pool)



def getMessageSummary(response):
    dates = [x['value'] for x in response.get('payload', {}).get('headers', []) if 'date' in x['name'].lower()]
    if dates:
        date = dates[0]
    else:
        date = None
    snippet = response.get('snippet')
    return date, snippet


class BatchMessageGetter(GoogleBatchScheduler):

    def __init__(S, gmail, batchSize=None, pool=None):
        super().__init__(batchSize, pool)
        S._logger = logger.getChild(type(S).__name__)
        S.gmail = gmail
        S._messagesAPI = gmail.users().messages()

    def _prepareRequest(S, ref):
        return S._messagesAPI.get(userId=ref.src.get('userId', 'me'), id=ref.src['id'])

    def _logResult(S, result):
        if result.payload.exception:
            S._logger.debug(f'Message {result.task.ref.idx}: {result.task.ref.src!r} ERROR: {result.payload.exception}')
        else:
            date, snippet = getMessageSummary(result.payload.response)
            S._logger.debug(f'Message {result.task.ref.idx}: {result.task.ref.src!r} [{date}] {snippet!r}')


_ = Gmail


if __name__ == '__main__':
    from my.utils.logging import setupLogger
    _logger = logging.getLogger('my.google')
    setupLogger(_logger)
    logger = logger.getChild('gmail')

    Gmail()

    print('ok')
