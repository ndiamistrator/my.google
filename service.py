from . import threadsafe

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

import sys
import pickle
import logging


__all__ = ['Service']


TOKENS = 'tokens.pickle'
CONFIG = 'credentials.json'


logger = logging.getLogger(__name__)


class Service:

    SERVICE_ID = {'serviceName':NotImplemented, 'version':NotImplemented}
    SCOPES = [NotImplemented]


    def __init__(S, scopes=None, config=None, tokens=None):
        threadsafe.patch()
        S._logger = logger.getChild(type(S).__name__)

        S.service = None

        if config is None:
            S.config = CONFIG
        else:
            S.config = config

        if scopes is None:
            S.scopes = tuple(S.SCOPES)
        else:
            S.scopes = tuple(scopes)

        if tokens is None:
            S.tokens = TOKENS
        else:
            S.tokens = tokens

        S.credentials = None

        if not S.tokens is False:
            try:
                with open(S.tokens, 'rb') as __tokens:
                    _tokens = pickle.load(__tokens)
            except FileNotFoundError:
                _tokens = {}
                S.credentials = None
            else:
                try:
                    S.credentials = _tokens[S.scopes]
                except KeyError:
                    pass

        if not S.credentials or not S.credentials.valid:
            if S.credentials and S.credentials.expired and S.credentials.refresh_token:
                S.credentials.refresh(Request())
            else:
                if isinstance(S.config, str): 
                    flow = InstalledAppFlow.from_client_secrets_file(S.config, scopes=S.scopes)
                else:
                    flow = InstalledAppFlow.from_client_config(S.config, scopes=S.scopes)
                S.credentials = flow.run_local_server(port=0)

            if not S.tokens is False:
                _tokens[S.scopes] = S.credentials
                with open(S.tokens, 'wb') as __tokens:
                    pickle.dump(_tokens, __tokens)

        S.service = build(credentials=S.credentials, **S.SERVICE_ID)


    def __getattr__(S, attr):
        return getattr(S.service, attr)

    # def __dir__(S):
    #     return set(super().__dir__()) | set(dir(S.service))


_ = Service


if __name__ == '__main__':
    from my.utils.logging import setupLogger
    setupLogger(logging.getLogger('my.google'))

    scopes = sys.argv[1:]
    Service(scopes)

    print('ok')
