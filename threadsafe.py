def patch():
    from .httplib2_patch import patch
    patch()

### import threading
### import logging
### 
### import googleapiclient.http
### # import googleapiclient.errors
### import google_auth_httplib2
### 
### 
### __all__ = ['HttpRequestMixIn', 'patch']
### 
### 
### logger = logging.getLogger(__name__)
### 
### 
### def debug_thread(tag):
###     thread = threading.current_thread()
###     logger.debug(f'thread {thread}: {tag}')
### 
### def debugHttpRequest(tag):
###     debug_thread(f'HttpRequest: {tag}')
### 
### def debugBatchHttpRequest(tag):
###     debug_thread(f'BatchHttpRequest: {tag}')
### 
### 
### # class HttpRequestMixIn:
### #     def execute(S, http=None, *args, **kwargs):
### 
### 
### def patched(execute):
###     try:
###         if not execute.__name__ == 'executeThreadSafe':
###             raise AttributeError
###     except AttributeError:
###         debug_thread(f'patching: {execute}')
###     else:
###         # debug_thread(f'already patched: {execute}')
###         return True
### 
### 
### def patch_HttpRequest():
###     __execute = googleapiclient.http.HttpRequest.execute
### 
###     if patched(__execute):
###         return
### 
###     def executeThreadSafe(S, given=None, *, num_retries=0):
###         if given is None and isinstance(S.http, google_auth_httplib2.AuthorizedHttp):
###             http = S.http
###             source = 'self'
###         elif given is None:
### 
###         elif isinstance(given, google_auth_httplib2.AuthorizedHttp):
###             http = given
###             source = 'given'
###         else:
###             typeName = type(given).__name__
###             givenStr = str(given)
###             if typeName not in givenStr:
###                 givenStr = f'{{{typeName}}} {givenStr}'
###             debugHttpRequest(f'not AuthorizedHttp (given): {givenStr}')
###             return __execute(S, http=given, num_retries=num_retries)
### 
###             # debugHttpRequest('given:', http, getattr(http, 'http', 'no http.http'))
### 
###         try:
###             _http = http.http
###         except AttributeError:
###             debugHttpRequest(f'no {source}.http.http attr: {http}') # TODO: maybe not a problem?
###             return __execute(S, http=given, num_retries=num_retries)
### 
###         if isinstance(_http, google_auth_httplib2.AuthorizedHttp):
###             _http = _http.http
### 
###         thread = threading.current_thread()
### 
###         try:
###             safethread = _http._safethread
###         except AttributeError:
###             __http = googleapiclient.http.build_http()
###             debugHttpRequest(f'no {source}.http._safethread attr: {http} . {_http} ; built {__http}')
###             __http._safethread = thread
###             http.http = __http
###         else:
###             if thread != safethread:
###                 __http = googleapiclient.http.build_http()
###                 debugHttpRequest(f'not safethread {source}.http: {http} . {_http} ; {safethread} ; built {__http}')
###                 __http._safethread = thread
###                 http.http = __http
### 
###         return __execute(S, http=http, num_retries=num_retries)
###         # try:
###         #     return __execute(S, *args, http=http, **kwargs)
###         # 
###         # except googleapiclient.errors.HttpError as E:
###         #     if getattr(E, 'resp', {}).get('status') == '401':
###         #         debugHttpRequest('retrying: 401')
###         #     else:
###         #         raise
###         # 
###         # return __execute(S, *args, http=None, **kwargs)
### 
###     googleapiclient.http.HttpRequest.execute = executeThreadSafe
### 
### 
### def patch_BatchHttpRequest():
###     __execute = googleapiclient.http.BatchHttpRequest.execute
### 
###     if patched(__execute):
###         return
### 
###     def executeThreadSafe(S, http=None, *, num_retries=0):
###         http = googleapiclient.http.build_http()
###         debugBatchHttpRequest(f'built (for batch): {http}')
###         return __execute(S, http=http)
### 
###     googleapiclient.http.BatchHttpRequest.execute = executeThreadSafe
### 
### 
### def patch():
###     patch_HttpRequest()
###     patch_BatchHttpRequest()
### 
###     # if HttpRequestMixIn not in googleapiclient.http.HttpRequest.__bases__:
###     #     debug_thread('patching', 'HttpRequest')
###     #     class HttpRequestThreadSafe(HttpRequestMixIn, googleapiclient.http.HttpRequest): pass
###     #     googleapiclient.http.HttpRequest = HttpRequestThreadSafe
###     # if HttpRequestMixIn not in googleapiclient.http.BatchHttpRequest.__bases__:
###     #     debug_thread('patching', 'BatchHttpRequest')
###     #     class BatchHttpRequestThreadSafe(HttpRequestMixIn, googleapiclient.http.BatchHttpRequest): pass
###     #     googleapiclient.http.BatchHttpRequest = BatchHttpRequestThreadSafe
### 
### 
### # Thread Safety
### #
### # https://github.com/googleapis/google-api-python-client/issues/687
### #
### # The httplib2.Http() objects are not thread-safe
### # 
### # The google-api-python-client library is built on top of the httplib2 library, which is not thread-safe. Therefore, if you are running as a multi-threaded application, each thread that you are making requests from must have its own instance of httplib2.Http().
### # 
### # The easiest way to provide threads with their own httplib2.Http() instances is to either override the construction of it within the service object or to pass an instance via the http argument to method calls. 
### #
### #   # Create a new Http() object for every request
### #   def build_request(http, *args, **kwargs):
### #     new_http = httplib2.Http()
### #     return googleapiclient.http.HttpRequest(new_http, *args, **kwargs)
### #   service = build('api_name', 'api_version', requestBuilder=build_request)
### # 
### #   # Pass in a new Http() manually for every request
### #   service = build('api_name', 'api_version')
### #   http = httplib2.Http()
### #   service.stamps().list().execute(http=http)
### #
### # Credential Storage objects are thread-safe
### # 
### # All Storage objects defined in this library are thread-safe, and multiple processes and threads can operate on a single store.
### # 
### # In some cases, this library automatically refreshes expired OAuth 2.0 tokens. When many threads are sharing the same set of credentials, the threads cooperate to minimize the total number of token refresh requests sent to the server. 
