from .service import Service
from .batch import GoogleBatchScheduler

import logging


__all__ = ['AVAILABLE_SCOPES', 'Drive']


AVAILABLE_SCOPES = [
    'https://www.googleapis.com/auth/drive.metadata.readonly',
    'https://www.googleapis.com/auth/drive',
    ]


logger = logging.getLogger(__name__)


class Drive(Service):

    SERVICE_ID = {'serviceName':'drive', 'version':'v3'}
    SCOPES = ['https://www.googleapis.com/auth/drive']
    MAX_SEARCH_PER_PAGE = 500 # gmail limit

    def test(S):
        results = S.files().list(
            pageSize=10, fields="nextPageToken, files(id, name)").execute()
        items = results.get('files', [])

        if not items:
            print('No files found.')
        else:
            print('Files:')
            for item in items:
                print(u'{0} ({1})'.format(item['name'], item['id']))


    def search(S, query,
            fields='nextPageToken, files(id, name)',
            perPage=None, maxPages=0, nextPageToken=None):
        for response in S.searchPaged(query, fields=fields, perPage=perPage, maxPages=maxPages, nextPageToken=nextPageToken):
            yield from response['files'] # {'id': ..., 'threadId': ...}



    def searchPaged(S, query,
            fields='nextPageToken, files(id, name)',
            perPage=None, maxPages=0, nextPageToken=None):
        # perPage: pageSize
        # nextPageToken: pageToken
        # q: q
        # 
        # orderBy
        #   createdTime
        #   folder
        #   modifiedByMeTime
        #   modifiedTime
        #   name
        #   name_natural
        #   quotaBytesUsed
        #   recency
        #   sharedWithMeTime
        #   starred
        #   viewedByMeTime'.
        #   orderBy='folder,modifiedTime desc,name'

        S._logger.debug(f'starting query: {query!r} (perPage={perPage}, maxPages={maxPages})')
        request = None
        page = 1
        if perPage is None:
            perPage = S.MAX_SEARCH_PER_PAGE
        maxPages = int(maxPages)
        count = 0

        while True:
            request = S.service.files().list(q=query, fields=fields, pageSize=perPage, pageToken=nextPageToken)
            response = request.execute()
            files = response.get('files', [])

            if not files:
                break

            _count = len(files)
            count += _count
            S._logger.debug(f'found {_count} file{"s" if _count>1 else ""} in page {page}...')

            yield response

            if page == maxPages:
                break

            nextPageToken = response.get('nextPageToken', None)
            if nextPageToken is None:
                break

            page += 1

        if not count:
            S._logger.debug(f'TOTAL: found no messages')
        else:
            S._logger.debug(f'TOTAL: found {count} file{"s" if count>1 else ""} in {page} page{"s" if page>1 else ""}')



_ = Drive
