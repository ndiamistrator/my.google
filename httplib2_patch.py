from threading import currentThread
import httplib2
import logging


logger = logging.getLogger(__name__)


class HttpThreadSafe:
    def __init__(
        S,
        cache=None,
        timeout=None,
        proxy_info=httplib2.proxy_info_from_environment,
        ca_certs=None,
        disable_ssl_certificate_validation=False,
        tls_maximum_version=None,
        tls_minimum_version=None,
    ):
        kwargs = dict(locals())
        del kwargs['S']
        object.__setattr__(S, '_HttpThreadSafe__threads', {currentThread(): Http(**kwargs)})
        del kwargs['cache']
        object.__setattr__(S, '_HttpThreadSafe__kwargs', kwargs)


    def __getInstance(S, meth):
        thread = currentThread()
        try:
            return S.__threads[thread]
        except KeyError:
            instance = type(S)()
            logger.warning(f'HttpThreadSafe.{meth} new instance for {thread}: {instance} ; was: {S}')
            S.__threads[thread] = instance
            return instance

    def __getattr__(S, attr):
        return getattr(S.__getInstance(f'__getattr__({attr})'), attr)
    def __setattr__(S, attr, value):
        return setattr(S.__getInstance(f'__setattr__({attr})'), attr, value)
    def __delattr__(S, attr):
        return delattr(S.__getInstance(f'__delattr__({attr})'), attr)
    def __getstate__(S):
        return S.__getInstance('__getstate__').__getstate__()
    def __setstate__(S, state):
        return S.__getInstance('__setstate__').__setstate__(state)


def patch():
    global Http

    if not issubclass(httplib2.Http, HttpThreadSafe):
        Http = httplib2.Http
        httplib2.Http = HttpThreadSafe
